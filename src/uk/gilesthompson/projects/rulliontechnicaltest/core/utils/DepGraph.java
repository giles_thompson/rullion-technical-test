/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.gilesthompson.projects.rulliontechnicaltest.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * A Dependency Graph implementation.
 * @author giles
 * @param <T> Vertex Id type.

 */
public class DepGraph<T> {
    
    private final List<Vertex> adjList;
    
    private final List<Vertex> ord;

    /** Creates new Dependency Graph instance.*/
    public DepGraph() {
        
        this.adjList = new ArrayList<>();
        this.ord = new ArrayList<>();
    }
   
    /** Appends the specified vertex to this depedency graph instance.
     * @param aVertex A vertex to append.*/
    public void addVertex(Vertex aVertex){
        
        this.adjList.add(aVertex);
    }
    
    /**
     * Addends verticies to this dependency graph instance.
     * @param vertices The list of verticies to append.
     */
    public void addVertices(List vertices){
        
        vertices.forEach((v) -> {this.addVertex((Vertex)v);});
    }
    
    /** Appends and maps edge to this dependency graph
     * @param anEdge. An edge to append*/
    public void addEdge(Edge anEdge){
        
        Object[] fromVtx = this.findVertex(anEdge.getFrom());
        Object[] toVtx = this.findVertex(anEdge.getTo());
        
        if(((Integer)fromVtx[0]) > ((Integer)toVtx[0]))
            throw new IllegalArgumentException("Cyclic references are not permitted!");
        
        ((Vertex)fromVtx[1]).addEdge(anEdge);
            

    }
    
    /**
     * Adds and maps the provided list of edges to the 
     * vertices in this dependency graph instance.
     * @param edges A List odf edges to append.
     */
    public void addEdges(List edges){
        
        edges.forEach((e)->{ this.addEdge((Edge)e);});
    }
    
    /**
     * List all vertices in this dependency graph.
     * @param ordered A boolean value which denotes whether or not the listing
     *                of the vertices are to be ordered according to their 
     *                respective dependencies.
     * @return List A List of all (optionally ordered) verticies in this dependency graph.
     */
    public List ListVertices(Boolean ordered){
        
        if(ordered){
            
            this.ord.clear();
            
            this.adjList.forEach((v)-> this.insertInOrder(v));
            
            return ord;
        }
        else
            return new ArrayList(this.adjList); //defensive
    }
    
    /**
     * @return int The total Vertices in this dependency graph.
    */
    public int totalVertices(){
        
        return this.adjList.size();
    }
    
    /**
     * @return int The total edges in this Depedency Graph.  
     */
    public int totalEdges(){
        
        int edgeCount = 0;
        edgeCount = this.adjList.stream().map((v) -> v.getEdgeCount()).reduce(edgeCount, Integer::sum);
        
        return edgeCount;
    }
    
    public void clear(){
        
        this.adjList.clear();
        this.ord.clear();
    }
    
    public Vertex newVertex(T id){
        
        return new Vertex(id);
    }
    
    public Edge newEdge(T from, T to){
        
        return new Edge(from,to);
    }
    
    private Object[] findVertex(T id){
        
        int idx = -1;
        Vertex targetVtx = null;
        for(int i = 0; i < this.adjList.size(); i++){
            if((targetVtx = this.adjList.get(i)).id.equals(id)){
                idx = i;
                break;
            }
        }
          
        if(idx == -1)
            throw new NoSuchElementException("The specified Vertex could not be found!");
        
        Object[] vertData = new Object[2];
        vertData[0] = idx;
        vertData[1] = targetVtx;
        
        return vertData;
    }
    
    private void insertInOrder(Vertex v){
        
        if(this.ord.contains(v))
            return;
        
        if(v.edges.isEmpty())
            this.ord.add(v);
       
        else{
            
            v.edges.forEach((e)->{insertInOrder((Vertex)this.findVertex(e.getTo())[1]);});
            this.ord.add(v);
            
        }
    }
    
    
    
    
    /** Models a single graph vertex. */
    public class Vertex{
        
        private final T id;
        private final List<Edge> edges;

        public Vertex(T id) {
            
            this.id = id;
            this.edges = new ArrayList<>();
        }
        
        public void addEdge(Edge edge){
            
            this.edges.add(edge);
        }
        
        public int getEdgeCount(){
            
            return this.edges.size();
        }

        public T getId() {
            return id;
        }
        
        
    }
    
    /** Models a single graph edge.*/
    public class Edge{
        
        private final T from,to;

        public Edge(T from, T to) {
            this.from = from;
            this.to = to;
        }

        public T getFrom() {
            return from;
        }

        public T getTo() {
            return to;
        }
        
       
        
    }
    
}     

