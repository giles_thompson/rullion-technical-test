/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.gilesthompson.projects.rulliontechnicaltest.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import uk.gilesthompson.projects.rulliontechnicaltest.core.utils.DepGraph;
import uk.gilesthompson.projects.rulliontechnicaltest.core.utils.DepGraph.Edge;
import uk.gilesthompson.projects.rulliontechnicaltest.core.utils.DepGraph.Vertex;

/**
 * TaskScheduler core, provided with a series of task this application
 * will order them such that all task depedencies appear before their
 * respective parent tasks.
 * @author giles
 */
public class TaskScheduler {
    
    private final DepGraph<String> dgraph;
    
    

    public TaskScheduler() {
        
        this.dgraph = new DepGraph<>();
    }
    
    /** Creates an ordered schedule from the specified entries
        taking into account their respective dependencies.
     * @param entries An Array of entries to schedule. 
     * @param dependencies The dependencies that exist between these entries which
     *                     should take the following form: A=>B to indicate that A depends
     *                     on B in this case.
     * @return An ordered list of entries taking into account their dependencies. Depedencies
     *         will ALWAYS be listed ahead of the entries that depend on them.
     * 
     * @throws IllegalArgumentException Where one or more cyclic dependencies are detected.
     *                     
     */
    public String[] generateOrderedSchedule(String[] entries,String[] dependencies){
        
        List<Vertex> vertices = this.parseEntries(entries);
        List<Edge> edges = this.parseDependencies(dependencies);
        this.dgraph.clear();
        this.dgraph.addVertices(vertices);
        this.dgraph.addEdges(edges);
        return this.toStringArr(this.dgraph.ListVertices(true)); 
    }
    
    
    private List<Edge> parseDependencies(String[] dependencies){
        
        final List<Edge> edges = new ArrayList<>();
        
        for(String curDependStr : dependencies){
            
            edges.add(this.parseDependency(curDependStr));
            
        }
        
        return edges;
    }
    
    private Edge parseDependency(String dependencyStr){
        
        String[] depComp = dependencyStr.split("=");
        
        Edge edge = this.dgraph.newEdge(depComp[0],depComp[1].substring(1));
        
        return edge;
    }
    
    private List<Vertex> parseEntries(String[] entries){
        
        final List<Vertex> vertices = new ArrayList<>(); 
        
        for(String curEntry : entries)
            vertices.add(this.dgraph.newVertex(curEntry));
            
        return vertices;
    }
    
    private String[] toStringArr(List<Vertex> vertices){
        
        String[] vertIdArr = new String[vertices.size()];
        for(int i = 0; i < vertices.size(); i++)
            vertIdArr[i] = (String)vertices.get(i).getId();
        return vertIdArr;
    }
    
    /**Application entry point.
     * @param args An array of Tasks and Dependencies which will be derived 
                   from a space delimited list of commmand line arguments 
                   that MUST take the following form:
                   Task1 Task2...TaskN $ "DEP1" "DEP2"... "DEPN"
                   (e.g A B C "D $ "A=>B" "B=>C") Note that quotations MUST be used around
                   the dependency definitions on the command line.
                   The array will be parsed into its constituent Task/Dependency
                   parsts before being passed to the TaskScheduler instance for execution.
                   NB: Task and Dependency arguments MUST be separated by a "$" literal on the command line.*/
    public static void main(String[] args){
        /**Firstly we prepare raw input data received from the command line
           to be passed in to our TaskScheduler instance which essentially involves
           separating Task and Depedency arguments into distinct arrays,
           once done we then create a new TaskScheduler instance and pass in the
            parsed Task and Dependency data.*/
        final List<String> tasksList = new ArrayList<>();
        final List<String> depList = new ArrayList<>();
        boolean tasksWritten = false;
        for(String curArg : args){
            if(curArg.contains("$")){
                tasksWritten = true;
                continue;
            }
            
            if(tasksWritten)
                depList.add(curArg);
            else
                tasksList.add(curArg);
        }
        final String[] tasksArr = tasksList.toArray(new String[tasksList.size()]);
        final String[] depsArr = depList.toArray(new String[depList.size()]);    
        final TaskScheduler scheduler = new TaskScheduler();
        System.out.println("Ordered Schedule: "+Arrays.toString(scheduler.generateOrderedSchedule(tasksArr, depsArr)));
    }
    
}
