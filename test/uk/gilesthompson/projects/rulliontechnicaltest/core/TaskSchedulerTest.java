/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.gilesthompson.projects.rulliontechnicaltest.core;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author giles
 */
public class TaskSchedulerTest {
    
    public TaskSchedulerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: []
     * Dependencies[]
     */
    @Test
    public void testGenerateOrderedSchedule1() {
        
        String[] entries = new String[0];
        String[] dependencies = new String[0];
        TaskScheduler instance = new TaskScheduler();
        String[] expResult = new String[0];
        String[] result = instance.generateOrderedSchedule(entries, dependencies);
        System.out.println(Arrays.toString(result));
        assertArrayEquals(expResult, result);
        
    }
    
    /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: ["A","B"]
     * Dependencies[]
     */
    @Test
    public void testGenerateOrderedSchedule2() {
        
        String[] entries = new String[2];
        entries[0] = "A";
        entries[1] = "B";    
        String[] dependencies = new String[0];
        TaskScheduler instance = new TaskScheduler();
        String[] expResult = new String[]{"A","B"};
        String[] result = instance.generateOrderedSchedule(entries, dependencies);
        System.out.println(Arrays.toString(result));
        assertArrayEquals(expResult, result);
        
    }
    
     /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: ["A","B"]
     * Dependencies["A=>B"]
     */
    @Test
    public void testGenerateOrderedSchedule3() {
        
        String[] entries = new String[2];
        entries[0] = "A";
        entries[1] = "B";    
        String[] dependencies = new String[]{"A=>B"};
        TaskScheduler instance = new TaskScheduler();
        String[] expResult = new String[]{"B","A"};
        String[] result = instance.generateOrderedSchedule(entries, dependencies);
        System.out.println(Arrays.toString(result));
        assertArrayEquals(expResult, result);
        
    }
    
     /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: ["A","B","C","D"]
     * Dependencies["A=>B","C=>D"]
     */
    @Test
    public void testGenerateOrderedSchedule4() {
        
        String[] entries = new String[4];
        entries[0] = "A";
        entries[1] = "B"; 
        entries[2] = "C";
        entries[3] = "D"; 
        String[] dependencies = new String[]{"A=>B","C=>D"};
        TaskScheduler instance = new TaskScheduler();
        String[] expResult = new String[]{"B","A","D","C"};
        String[] result = instance.generateOrderedSchedule(entries, dependencies);
        System.out.println(Arrays.toString(result));
        assertArrayEquals(expResult, result);
        
    }
    
    
    /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: ["A","B","C"]
     * Dependencies["A=>B","B=>C"]
     */
    @Test
    public void testGenerateOrderedSchedule5() {
        
        String[] entries = new String[3];
        entries[0] = "A";
        entries[1] = "B"; 
        entries[2] = "C";
        String[] dependencies = new String[]{"A=>B","B=>C"};
        TaskScheduler instance = new TaskScheduler();
        String[] expResult = new String[]{"C","B","A"};
        String[] result = instance.generateOrderedSchedule(entries, dependencies);
        System.out.println(Arrays.toString(result));
        assertArrayEquals(expResult, result);
        
    }
    
    /**
     * Test of generateOrderedSchedule method, of class TaskScheduler.
     * Entries: Entries: ["A","B","C","D"]
     * Dependencies["A=>B","B=>C","C=>A]
     */
    @Test(expected= IllegalArgumentException.class) //on account of cyclic dependency.
    public void testGenerateOrderedSchedule6() {
        
        String[] entries = new String[4];
        entries[0] = "A";
        entries[1] = "B"; 
        entries[2] = "C";
        entries[3] = "D";
        String[] dependencies = new String[]{"A=>B","B=>C","C=>A"};
        TaskScheduler instance = new TaskScheduler();
        //we catch the exception just to print its message to the command line.
        //then proceed to rethrow it. This is necessary in order for this test to pass
        try{
        instance.generateOrderedSchedule(entries, dependencies);
        }
        catch(Exception e){
        System.out.println(e.getMessage());
        throw e;
        
        }
        
    }
    
}
